// -*- mode:c++; tab-width:2; indent-tabs-mode:nil;  -*-
/**
 *  \file listrank-cilk.cc
 *
 *  \brief Implement the 'listrank-par.hh' interface using Cilk Plus.
 */

#include <cassert>
#include <cstring>
#include <cmath>

#include <algorithm>
#include <iostream>

#include "listrank-par.hh"

using namespace std;

// ============================================================
const char *
getImplName__par (void)
{
  return "CILK";
}

// ============================================================

struct ParRankedList_t__
{
  size_t n;
  const index_t* Next;
  rank_t* Rank;
};

// ============================================================

ParRankedList_t *
setupRanks__par (size_t n, const index_t* Next)
{
  ParRankedList_t* L = new ParRankedList_t;
  assert (L);

  L->n = n;
  L->Next = Next;
  L->Rank = createRanksBuffer (n);

  return L;
}

void releaseRanks__par (ParRankedList_t* L)
{
  if (L) {
    releaseRanksBuffer (L->Rank);
  }
}

// ============================================================

const rank_t *
getRanks__par (const ParRankedList_t* L)
{
  return L->Rank;
}

// ============================================================

static void
computeListRanks__cilk__ (size_t n, const index_t* Next, rank_t* Rank)
{
  if (n == 0) return; // empty pool
  assert (Next);
  assert (Rank);

  // Initial values on which we will perform the list-based 'scan' /
  // 'prefix sum'
  _Cilk_for (size_t i = 0; i < n; ++i)
    Rank[i] = (Next[i] == NIL) ? 0 : 1;

  //------------------------------------------------------------
  //
  // ... YOUR CODE GOES HERE ...
  //
  index_t* NNext = (index_t*)malloc(n*sizeof(index_t));
  _Cilk_for (size_t i=0; i < n; ++i)
    NNext[i] = Next[i];

  index_t* CNext = (index_t*)malloc(n*sizeof(index_t));
  rank_t* CRank = (rank_t*)malloc(n*sizeof(rank_t));
  rank_t* NRank = Rank;

  for(int j=0; j<ceil(log(n)/log(2)); ++j) {
    index_t* temp = CNext;
    CNext = NNext;
    NNext = temp;

    rank_t* temp2 = CRank;
    CRank = NRank;
    NRank = temp2;

    _Cilk_for (size_t i = 0; i < n; ++i) {
      if(CNext[i] != NIL) {
        NRank[i] = CRank[i] + CRank[CNext[i]];
        NNext[i] = CNext[CNext[i]];
      } else {
        NRank[i] = CRank[i];
        NNext[i] = NIL;
      }
    }
  }

  if(Rank != NRank) {
    index_t* temp = CNext;
    CNext = NNext;
    NNext = temp;

    rank_t* temp2 = CRank;
    CRank = NRank;
    NRank = temp2;

    _Cilk_for (size_t i=0; i < n; ++i)
      Rank[i] = CRank[i];
  }

  free(NNext);
  free(CNext);
  free(CRank);

  // (you may also modify any of the preceding code if you wish)
  //
  //#include "soln--cilk.cc" // Instructor's solution: none for you!
  //------------------------------------------------------------
}

void
computeListRanks__par (ParRankedList_t* L)
{
  assert (L != NULL);
  computeListRanks__cilk__ (L->n, L->Next, L->Rank);
}

// eof
