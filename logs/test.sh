!/bin/bash

make clean; make
./listrank-cilk 10000000 5 >> logs/log_cilk_10000000 2>&1
./listrank-cuda 10000000 5 >> logs/log_cuda_10000000 2>&1
./listrank-cilk 5234698 5 >> logs/log_cilk_5234698 2>&1
./listrank-cuda 5234698 5 >> logs/log_cuda_5234698 2>&1

